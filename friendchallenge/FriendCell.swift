//
//  FriendCell.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-26.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class FriendCell: UITableViewCell {

    @IBOutlet weak var friendLabel: UILabel!
}
