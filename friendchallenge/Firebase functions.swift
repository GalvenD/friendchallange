//
//  Firebase functions.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-26.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import Foundation
import Firebase

class FirebaseFunctions
{
    let fbRef = Database.database().reference()
    var checkBool : Bool?

    //Check if username is taken
    func checkUserName(child1 : String, child2 : String, completionHandler:@escaping (Bool, String, String) -> ())
    {
        fbRef.child(child1).child(child2).observeSingleEvent(of: .value, with: {(snapshot) in
            if(snapshot.exists() == true)
            {
                completionHandler(true, snapshot.value as! String, snapshot.key)
            } else {
                completionHandler(false, "", "")
            }
        }){(error) in
            self.errorWarning(error: error)
        }
    }
    
    //Setup userdata
    func setUserData(completionHandler:@escaping (Bool) -> ())
    {
        fbRef.child("Users").child((Auth.auth().currentUser?.uid)!).child("Userinfo").observeSingleEvent(of: .value, with: {(DataSnapShot) in
            
            let data = DataSnapShot.value as? NSDictionary
            LogInUser.sharedInstance.name = data?.value(forKey: "username") as? String
            LogInUser.sharedInstance.fbUID = Auth.auth().currentUser?.uid
            
            if(LogInUser.sharedInstance.name != nil && LogInUser.sharedInstance.fbUID != nil)
            {
                LogInUser.sharedInstance.dataFetched = true
                completionHandler(true)
            } else {
                print("något gick fel")
                completionHandler(false)
            }
        }){(error) in
            self.errorWarning(error: error)
        }
    }
    
    //Sign out
    func signOut(completionHandler: @escaping () -> ())
    {
        do
        {
            try Auth.auth().signOut()
            completionHandler()
        } catch {
            print("Gick inte att logga ut")
        }
    }
    
    //Errorhandeling
    func errorWarning(error: Error)
    {
        print("Error: " + error.localizedDescription)
    }
}
