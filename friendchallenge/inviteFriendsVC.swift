//
//  inviteFriendsVC.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-27.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class inviteFriendsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var loadingWheel: UIActivityIndicatorView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var inviteTableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        parentView.layer.cornerRadius = 10
        let clickNotification = Notification.Name("invitepress")
        NotificationCenter.default.addObserver(self, selector: #selector(invitePress), name: clickNotification, object: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        //loadFriendsList()
    }
    
    func loadingOn()
    {
        loadingWheel.startAnimating()
        loadingLabel.isHidden = false
        inviteTableView.isUserInteractionEnabled = false
        inviteTableView.alpha = 0.4
    }
    
    func loadingOff()
    {
        loadingWheel.stopAnimating()
        loadingWheel.isHidden = true
        inviteTableView.isUserInteractionEnabled = true
        inviteTableView.alpha = 1
    }
    
    func invitePress(notification : NSNotification)
    {
        let post = notification.object as! NSDictionary
        let invite = post.value(forKey: "invited") as! Bool
        let rowNumber = post.value(forKey: "rowNumber") as! Int
        
        LogInUser.sharedInstance.friends[rowNumber].inviteToChallenge = invite
    }
    
    func loadFriendsList()
    {
        FireBaseFriends().getFriends(completionHandler: { (pending, friends) in
            LogInUser.sharedInstance.friends = friends
            self.inviteTableView.reloadData()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LogInUser.sharedInstance.friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "invitecell") as! InviteCell
        cell.nameLabel.text = LogInUser.sharedInstance.friends[indexPath.row].name
        cell.rowNumber = indexPath.row
        return cell
    }
    
    @IBAction func doneClick(_ sender: Any)
    {
        loadingOn()
        uploadChallenge()
    }
    
    func inviteFirends(challengeToSend: String, completionHandler: @escaping () -> ())
    {
        Challenge.placeholderChallenge.participantsName.removeAll()
        
        //Send invites
        for friend in LogInUser.sharedInstance.friends
        {
            if(friend.inviteToChallenge == true)
            {
                FireBaseChallenges().sendChallengeInvite(isPrivate: Challenge.placeholderChallenge.isPrivate, friend: friend.friendId!, challenge: challengeToSend, title: Challenge.placeholderChallenge.headLine!)
            }
        }
        
        //Set challenge to user directory
        FireBaseChallenges().setChallengeToUser(isPrivate: Challenge.placeholderChallenge.isPrivate, challenge: challengeToSend, title: Challenge.placeholderChallenge.headLine!)
        
        completionHandler()
    }
    
    func uploadChallenge()
    {
        let challenge : [String : Any] = ["Title" : Challenge.placeholderChallenge.headLine!, "Rules" : Challenge.placeholderChallenge.rules!,
                                          "Steps" : Challenge.placeholderChallenge.stepsToComplete, "ChatEnabled" : Challenge.placeholderChallenge.chatActive,
                                          "CreatedBy" : LogInUser.sharedInstance.name!, "ParticipantsNr" : 1, "Completes" : 0, "Removed" : false]
        
            FireBaseChallenges().uploadChallenge(isPrivate: Challenge.placeholderChallenge.isPrivate, challengeData: challenge, completionHandler: { (challengeId) in
                self.inviteFirends(challengeToSend: challengeId, completionHandler: {
                    self.loadingOff()
                    self.presentingViewController!.presentingViewController!.dismiss(animated: true, completion: {})
                })
            })
    }
    
    
    override func viewDidDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
