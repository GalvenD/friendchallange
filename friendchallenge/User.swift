//
//  User.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-14.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import Foundation

public class LogInUser
{
    static var sharedInstance = LogInUser()
    
    var dataFetched = false
    
    var name : String?
    var fbUID : String?
    var nrChallenges : Int?
    
    var placements = [Int]()
    
    var friendRequests = [Friend]()
    var friends = [Friend]()
    
    var ongoingCH = [Challenge]()
    var pendingCH = [Challenge]()
    var finishedCH = [Challenge]()
    var publicCH = [Challenge]()
}
