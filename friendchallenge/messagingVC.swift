//
//  messagingVC.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-28.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit
import Firebase

class messagingVC: UIViewController, UITextViewDelegate, UITableViewDataSource, UITableViewDelegate
{

    @IBOutlet weak var sendButton: AddKey!
    @IBOutlet weak var loadWheel: UIActivityIndicatorView!
    @IBOutlet weak var moveView: UIView!
    @IBOutlet weak var messageTV: UITextView!
    @IBOutlet weak var tVHeight: NSLayoutConstraint!
    @IBOutlet weak var messageTableView: UITableView!

    var chatId : String?
    var chatDest : String?
    
    var fbRef = Database.database().reference()
    
    var messages = [Message]()
    var userMessage = Message()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        messageTV.delegate = self
        
        messageTableView.rowHeight = UITableViewAutomaticDimension
        messageTableView.estimatedRowHeight = 140
        messageTableView.delegate = self
        messageTableView.dataSource = self
        
        messageTableView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        addObserver()
    }
    
    func loadingOn()
    {
        loadWheel.startAnimating()
        sendButton.isHidden = true
    }
    
    func loadingOff()
    {
        loadWheel.stopAnimating()
        sendButton.isHidden = false
    }
    
    func keyboardWillShow(notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.moveView.frame.origin.y -= keyboardSize.height
        }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.moveView.frame.origin.y += keyboardSize.height
            tVHeight.constant = 30
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //User message
        if(messages[indexPath.row].sentBy! == LogInUser.sharedInstance.name!)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "usercell", for: indexPath) as! userChatCell
            cell.nameLabel.text = messages[indexPath.row].sentBy!
            cell.messageLabel.text = messages[indexPath.row].text!
            return cell
        } else { //Recieving message
            let cell = tableView.dequeueReusableCell(withIdentifier: "recieve", for: indexPath) as! recieveCell
            cell.nameLabel.text = messages[indexPath.row].sentBy!
            cell.messageLabel.text = messages[indexPath.row].text!
            return cell
        }
    }
    
    @IBAction func sendClick(_ sender: Any)
    {
        postMessage()
    }
    
    func reloadTableview()
    {
        self.userMessage = Message()
        self.view.endEditing(true)
        //messages.reverse()
        self.messageTV.text = ""
        self.messageTableView.reloadData()
    }
    
    func postMessage()
    {
        userMessage.sentBy = LogInUser.sharedInstance.name!
        userMessage.text = messageTV.text
        loadingOn()
        
        let value = ["SentBy" : userMessage.sentBy!, "Text" : userMessage.text!]
        let post = fbRef.child("Challenges").child(chatDest!).child(chatId!).child("Messages").childByAutoId()
        post.setValue(value, withCompletionBlock: { (error, refer) in
            if(error == nil)
            {
                self.userMessage.id = post.key
                self.loadingOff()
                //self.messages.append(self.userMessage)
                //self.reloadTableview()
            } else {
                self.loadingOff()
                print("något gick fel")
                //self.reloadTableview()
            }
        })
    }
    
    func addObserver()
    {
        let observe = fbRef.child("Challenges").child(chatDest!).child(chatId!).child("Messages")
        observe.observe(.childAdded, with: {(snapshot) in
            let request = snapshot.value as! NSDictionary
            let newMessage = Message()
            newMessage.id = snapshot.key
            newMessage.sentBy = request.value(forKey: "SentBy") as? String
            newMessage.text = request.value(forKey: "Text") as? String
            self.messages.insert(newMessage, at: 0)
            
            self.reloadTableview()
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        textViewSize()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        textViewSize()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            tVHeight.constant = tVHeight.constant + 17
        }
        return true
    }
    
    func textViewSize()
    {
        let size : CGSize = messageTV.contentSize
        tVHeight.constant = size.height
        messageTV.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
    }
    
    @IBAction func backClick(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        fbRef.removeAllObservers()
    }
}
