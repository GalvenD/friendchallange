//
//  FriendLabel.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-26.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

@IBDesignable
class FriendLabel: UILabel {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var maskToBounds: Bool = true {
        didSet{
            layer.masksToBounds = maskToBounds
        }
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
}
