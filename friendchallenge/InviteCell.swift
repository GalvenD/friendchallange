//
//  InviteCell.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-27.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class InviteCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var inviteKey: AddKey!
    
    var rowNumber : Int?
    var pressed = false
    var blueColor = UIColor(red:0.28, green:0.37, blue:1.00, alpha:1.0)

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func invitePress(_ sender: Any)
    {
        if(pressed == false)
        {
            pressed = true
            inviteKey.borderWidth = 0
            inviteKey.backgroundColor = blueColor
            inviteKey.setTitle("Invited", for: .normal)
            inviteKey.setTitleColor(UIColor.white, for: .normal)
        } else {
            pressed = false
            inviteKey.borderWidth = 2
            inviteKey.backgroundColor = UIColor.white
            inviteKey.setTitle("Invite", for: .normal)
            inviteKey.setTitleColor(blueColor, for: .normal)
        }
        let inviteData = ["rowNumber" : rowNumber!, "invited" : pressed] as [String : Any]
        let passRownumber = Notification.Name("invitepress")
        NotificationCenter.default.post(name: passRownumber, object: inviteData)
    }

}
