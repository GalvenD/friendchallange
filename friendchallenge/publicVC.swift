//
//  publicVC.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-07-02.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class publicVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var publicTV: UITableView!
    
    var refreshControl: UIRefreshControl!
    
    var dontLoad = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.updatePublicList), for: UIControlEvents.valueChanged)
        publicTV.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if(dontLoad == false)
        {
            updatePublicList()
        }
    }
    
    func updatePublicList()
    {
        FireBaseChallenges().getPublicChallenges(completion: {(challenges) in
            LogInUser.sharedInstance.publicCH = challenges
            self.publicTV.reloadData()
            self.refreshControl.endRefreshing()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LogInUser.sharedInstance.publicCH.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "public", for: indexPath) as! joinPublicCell
        cell.titleLabel.text = LogInUser.sharedInstance.publicCH[indexPath.row].headLine!
        cell.participantsLabel.text = "Participants: " + String(LogInUser.sharedInstance.publicCH[indexPath.row].participantsNr)
        cell.createdByLabel.text = "Created by: " + LogInUser.sharedInstance.publicCH[indexPath.row].createdBy!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "publicDetail", sender: LogInUser.sharedInstance.publicCH[indexPath.row])
    }
    
    @IBAction func doneClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dest = segue.destination as! detailChallengeVC
        dest.challenge = sender as! Challenge
        dest.fromPublic = true
        dontLoad = true
    }
}
