//
//  AddKey.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-14.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

@IBDesignable
class AddKey: UIButton {
    
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var maskToBounds: Bool = true {
        didSet{
            layer.masksToBounds = maskToBounds
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
