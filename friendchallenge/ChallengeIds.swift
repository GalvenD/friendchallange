//
//  ChallengeIds.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-29.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import Foundation

class ChallengeIds
{
    var key : String?
    var isPrivate : Bool?
    var title : String?
}
