//
//  Alertcontroller.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-22.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import Foundation
import UIKit

class AlertController : UIAlertController
{
    var alertController : UIAlertController?
    var oneAction : UIAlertAction?
    var twoAction : UIAlertAction?
    
    func oneActionFunc(ftitle : String, fmessage : String) -> UIAlertController
    {
        alertController = UIAlertController()
        alertController?.title = ftitle
        alertController?.message = fmessage
        oneAction = UIAlertAction(title: "OK", style: .default){ _ in
            return
        }
        alertController?.addAction(oneAction!)
        return alertController!
    }
    
    func twoAction(ftitle: String, fmessage : String, completion: @escaping () -> ()) -> UIAlertController
    {
        alertController = UIAlertController()
        alertController?.title = ftitle
        alertController?.message = fmessage
        
        oneAction = UIAlertAction(title: "OK", style: .destructive){_ in
            completion()
        }
        twoAction = UIAlertAction(title: "Cancel", style: .cancel){_ in
            return
        }
        alertController?.addAction(oneAction!)
        alertController?.addAction(twoAction!)
        return alertController!
    }
}
