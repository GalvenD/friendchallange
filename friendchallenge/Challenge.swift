//
//  Challenge.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-14.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import Foundation

class Challenge
{
    static var placeholderChallenge = Challenge()
    
    var id : String?
    var createdBy : String?
    
    var headLine : String?
    var rules : String?
    var isPrivate = true
    var stepsToComplete = 1
    var chatActive = true
    var publicPrivate : String?
    
    var participantsNr = 1
    var completes = 0
    var participantsName = [String]()
    var blocked = false
    var removed = false
    
    /*
      state describes the state of the challenge
     0 = pending request
     1 = accepted and ongoing
     2 = Finished
     */
    var state : Int?
    var stepsDone = 0
    
    var arrayPlacement : Int?
}
