//
//  Friend.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-26.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import Foundation

class Friend
{
    var name : String?
    //var fbUID : String?
    var friendId : String?
    var accepted : Bool?
    
    var inviteToChallenge = false
}
