//
//  pendingCell.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-29.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class pendingCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var publicPrivateLabel: UILabel!
    
    var rowNumber : Int?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func acceptClick(_ sender: Any)
    {
        sendRow(accept: true)
    }
    
    @IBAction func ignoreClick(_ sender: Any)
    {
        sendRow(accept: false)
    }
    
    func sendRow(accept : Bool)
    {
        let passRownumber = Notification.Name("rowNumber")
        let message = ["rowNumber" : rowNumber as Any, "accept" : accept]
        NotificationCenter.default.post(name: passRownumber, object: message)
    }

}
