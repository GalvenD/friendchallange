//
//  addFriendVC.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-26.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class addFriendVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var displaySearchLabel: UILabel!
    @IBOutlet weak var sendRequestKey: AddKey!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var loadWheel: UIActivityIndicatorView!
    @IBOutlet weak var searchButton: AddKey!

    var firebase = FirebaseFunctions()
    
    var searchUID : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        displaySearchLabel.isHidden = true
        sendRequestKey.isHidden = true
        searchButton.isHidden = true
        parentView.layer.cornerRadius = 10
    }

    @IBAction func closeClick(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendRequestClick(_ sender: Any)
    {
        loadWheel.startAnimating()
        searchButton.isHidden = true
        searchTF.isEnabled = false
        sendRequestKey.isHidden = true
        searchTF.text = ""
        FireBaseFriends().sendFriendRequest(uid: searchUID!, value: ["Name" : LogInUser.sharedInstance.name!, "Accepted" : false], completionHandler:{ (done) in
            if(done == true)
            {
                self.loadWheel.stopAnimating()
                self.searchTF.isEnabled = true
                self.displaySearchLabel.text = "Request sent"
            } else {
                self.loadWheel.stopAnimating()
                self.searchTF.isEnabled = true
                self.displaySearchLabel.text = "You've already sent a request"
                self.searchTF.text = ""
            }
        })
    }

    @IBAction func searchClick(_ sender: Any)
    {
        view.endEditing(true)
        loadWheel.startAnimating()
        searchButton.isHidden = true
        searchTF.isEnabled = false
        displaySearchLabel.isHidden = false
        displaySearchLabel.text = "Searching..."
        sendRequestKey.isHidden = true
        firebase.checkUserName(child1: "Usernames", child2: searchTF.text!, completionHandler: {
            exists in
            if(exists.0 == true)
            {
                self.loadWheel.stopAnimating()
                self.displaySearchLabel.text = exists.2
                self.sendRequestKey.isHidden = false
                self.searchButton.isHidden = false
                self.searchTF.isEnabled = true
                self.searchUID = exists.1
            } else {
                self.loadWheel.stopAnimating()
                self.searchTF.isEnabled = true
                self.searchButton.isHidden = false
                self.displaySearchLabel.text = "Couldn't find user"
            }
        })
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == searchTF)
        {
            self.sendRequestKey.isHidden = true
            self.displaySearchLabel.isHidden = true
        }
        return true
    }
    
    @IBAction func valueChanged(_ sender: Any)
    {
        
    }

    @IBAction func editingChanged(_ sender: Any)
    {
        if(searchTF.text == "")
        {
            searchButton.isHidden = true
        } else {
            searchButton.isHidden = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField == searchTF)
        {
            view.endEditing(true)
        }
        return false
    }
}
