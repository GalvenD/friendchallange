//
//  Participant.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-07-01.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import Foundation

class Participant
{
    static var sharedInstance = Participant()
    var name : String?
    var id : String?
    
    var participant = [Participant]()
}
