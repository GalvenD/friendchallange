//
//  FireBaseFriends.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-07-11.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import Foundation
import Firebase

class FireBaseFriends
{
    
    let fbRef = Database.database().reference()
    
    //Get friends list
    func getFriends(completionHandler: @escaping ([Friend], [Friend]) -> ())
    {
        fbRef.child("Users").child((Auth.auth().currentUser?.uid)!).child("Friends").observeSingleEvent(of: .value, with: {(snapshot) in
            var temprequests = [Friend]()
            var tempfriend = [Friend]()
            
            for item in snapshot.children.allObjects as! [DataSnapshot]
            {
                let requestData = item.value as! NSDictionary
                let friendRequest = Friend()
                
                friendRequest.name = requestData.value(forKey: "Name") as? String
                //friendRequest.fbUID = requestData.value(forKey: "UID") as? String
                friendRequest.friendId = item.key
                friendRequest.accepted = requestData.value(forKey: "Accepted") as? Bool
                
                if(friendRequest.accepted == false)
                {
                    temprequests.append(friendRequest)
                } else {
                    tempfriend.append(friendRequest)
                }
            }
            DispatchQueue.main.async {
                completionHandler(temprequests, tempfriend)
            }
        })
    }
    
    //Send friend requests
    func sendFriendRequest(uid : String, value : [String : Any],completionHandler:@escaping (Bool) -> ())
    {
        fbRef.child("Users").child(uid).child("Friends").child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: {(snapshot) in
            if(snapshot.exists() == true)
            {
                completionHandler(false)
                print("redan frågat")
            } else {
                self.fbRef.child("Users").child(uid).child("Friends").child((Auth.auth().currentUser?.uid)!).setValue(value)
                completionHandler(true)
            }
        }){(error) in
            self.errorWarning(error: error)
        }
    }
    
    //Accept or ignore friend request
    func acceptIgnore(accept : Bool, fbId : String, completionHandler: @escaping (Bool) -> ())
    {
        //Users directory
        let action = fbRef.child("Users").child((Auth.auth().currentUser?.uid)!).child("Friends").child(fbId)
        //Friends directory
        let setFriend = fbRef.child("Users").child(fbId).child("Friends").child((Auth.auth().currentUser?.uid)!)
        
        //Accept request
        if(accept == true)
        {
            let accepted = ["Accepted" : true]
            
            //Publish your data to friends directory
            let friend = ["Name" : LogInUser.sharedInstance.name as Any, "Accepted" : true] as [String : Any]
            setFriend.updateChildValues(friend)
            
            //Publish accept to your data
            action.updateChildValues(accepted, withCompletionBlock: {(error, refer) in
                if(error == nil)
                {
                    completionHandler(true)
                } else {
                    completionHandler(false)
                    self.errorWarning(error: error!)
                }
            })
        } else { //Ignore request
            action.removeValue(completionBlock: {(error, refer) in
                if(error == nil)
                {
                    completionHandler(true)
                } else {
                    completionHandler(false)
                    self.errorWarning(error: error!)
                }
            })
        }
    }
    
    //Errorhandeling
    func errorWarning(error: Error)
    {
        print("Error: " + error.localizedDescription)
    }
}
