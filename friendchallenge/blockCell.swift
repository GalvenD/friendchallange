//
//  blockCell.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-07-01.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class blockCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    var rowNumber : Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func blockClick(_ sender: Any)
    {
        let passRownumber = Notification.Name("blockNumber")
        let message = ["blockNumber" : rowNumber as Any]
        NotificationCenter.default.post(name: passRownumber, object: message)
    }

}
