//
//  detailChallengeVC.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-30.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class detailChallengeVC: UIViewController, UIScrollViewDelegate {
    
    var fromPublic = false
    
    var challenge = Challenge()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rulesLabel: FriendLabel!
    @IBOutlet weak var participantsLabel: UILabel!
    @IBOutlet weak var completesLabel: UILabel!
    @IBOutlet weak var createdByLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var chatButton: AddKey!
    @IBOutlet weak var completeButton: AddKey!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var retireButton: AddKey!
    @IBOutlet weak var adminView: UIView!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var loadingWheel: UIActivityIndicatorView!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var backButton: AddKey!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var stepsLabel: UILabel!
    @IBOutlet weak var changeChatLabel: UILabel!
    @IBOutlet weak var saveChatSettingsButton: AddKey!
    @IBOutlet weak var chatSwitch: UISwitch!

    var alertController = AlertController()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        rulesLabel.layer.masksToBounds = true
        
        titleLabel.text = challenge.headLine!
        rulesLabel.text = "Rules: \n" + challenge.rules!
        participantsLabel.text = "Participants: " + String(challenge.participantsNr)
        completesLabel.text = "Completes: " + String(challenge.completes)
        createdByLabel.text = "Created by: " + challenge.createdBy!
        progressBar.setProgress((Float(challenge.stepsDone) / Float(challenge.stepsToComplete)), animated: true)
        stepsLabel.text = "Step completed: " + String(challenge.stepsDone) + "/" + String(challenge.stepsToComplete)
        setCompleteButtonTitle()
        blockedRemoved()
        print("FromPublic: " + String(fromPublic))
        titleLabel.sizeToFit()
        
        if(challenge.state == 0)
        {
            completeButton.setTitle("Join challenge", for: .normal)
            settingsButton.isHidden = true
            chatButton.isHidden = true
        }
            saveChatSettingsButton.isHidden = true
        
        if(challenge.chatActive == false)
        {
            chatButton.isHidden = true
        }
    }
    
    @IBAction func saveClick(_ sender: Any)
    {
        if(chatSwitch.isOn == true)
        {
            let value = ["ChatEnabled" : true]
            chatButton.isHidden = false
            challenge.chatActive = true
            checkChatSettings()
            FireBaseChallenges().changeChatSettings(value: value, id: challenge.id!, publicPrivate: challenge.publicPrivate!)
        } else {
            let value = ["ChatEnabled" : false]
            chatButton.isHidden = true
            challenge.chatActive = false
            checkChatSettings()
            FireBaseChallenges().changeChatSettings(value: value, id: challenge.id!, publicPrivate: challenge.publicPrivate!)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func blockedRemoved()
    {
        if(challenge.blocked == true || challenge.removed == true)
        {
            chatButton.isHidden = true
            completeButton.isHidden = true
            settingsButton.isHidden = true
            retireButton.isHidden = false
        }
    }
    
    func loadingOn()
    {
        loadingWheel.startAnimating()
        loadingLabel.isHidden = false
        scrollView.isUserInteractionEnabled = false
        scrollView.alpha = 0.4
    }
    
    func loadingOff()
    {
        loadingWheel.stopAnimating()
        loadingLabel.isHidden = true
        scrollView.isUserInteractionEnabled = true
        scrollView.alpha = 1
    }
    
    @IBAction func chatClick(_ sender: Any)
    {
        performSegue(withIdentifier: "chat", sender: challenge)
    }

    func setCompleteButtonTitle()
    {
        if(self.challenge.stepsDone == self.challenge.stepsToComplete - 1)
        {
            self.completeButton.setTitle("Complete challenge", for: .normal)
        } else{
            
            self.completeButton.setTitle("Complete step: " + String(self.challenge.stepsDone + 1), for: .normal)
        }
    }

    @IBAction func settingsClick(_ sender: Any)
    {
        checkChatSettings()
        UIView.animate(withDuration: 0.5, animations: {
            self.settingsButton.transform = self.settingsButton.transform.rotated(by: CGFloat(Double.pi))
            self.view.layoutIfNeeded()
        }, completion: {_ in
            if(self.retireButton.isHidden == true)
            {
                self.retireButton.isHidden = false
                if(self.challenge.createdBy == LogInUser.sharedInstance.name)
                {
                    self.adminView.isHidden = false
                    self.scrollView.isScrollEnabled = true
                }
            } else {
                self.retireButton.isHidden = true
                self.adminView.isHidden = true
                if(self.challenge.createdBy == LogInUser.sharedInstance.name)
                {
                    self.adminView.isHidden = true
                    self.scrollView.isScrollEnabled = false
                    self.scrollView.setContentOffset(CGPoint(x:0, y: 0), animated: true)
                }
            }
            
        })
    }

    @IBAction func backClick(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func completeStepClick(_ sender: Any)
    {
        loadingOn()
        if(completeButton.title(for: .normal) == "Join challenge")
        {
            FireBaseChallenges().joinPublic(id: challenge.id!, title: challenge.headLine!, user: LogInUser.sharedInstance.fbUID!, completion: {
                self.loadingOff()
                if(self.fromPublic == true)
                {
                    self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                } else {
                    self.dismiss(animated: true, completion: {})
                }
            })
        } else{
            print("else")
            
            completeButton.isEnabled = false
            challenge.stepsDone += 1
            if(challenge.stepsDone == challenge.stepsToComplete)
            {
                finished()
            } else {
                oneUp()
            }
        }
    }
    
    func oneUp()
    {
        FireBaseChallenges().updateSteps(id: challenge.id!, count: challenge.stepsDone, completionHandler: {
            self.loadingOff()
            CATransaction.begin()
            CATransaction.setCompletionBlock({
                self.completeButton.isEnabled = true
                self.setCompleteButtonTitle()
            })
            self.progressBar.setProgress((Float(self.challenge.stepsDone) / Float(self.challenge.stepsToComplete)), animated: true)
            CATransaction.commit()
        })
    }
    
    @IBAction func retireClick(_ sender: Any)
    {
        if(challenge.createdBy == LogInUser.sharedInstance.name)
        {
            self.present(alertController.twoAction(ftitle: "Warning", fmessage: "Do you want to retire from " + challenge.headLine! + "? Since you are the creator, this will completely remove the challenge.", completion: {
                self.loadingOn()
                FireBaseChallenges().retireFromChallenge(id: self.challenge.id!, publicPrivate: self.challenge.publicPrivate!, completion: {
                    FireBaseChallenges().removeChallenge(id: self.challenge.id!, publicPrivate: self.challenge.publicPrivate!, completion: {
                        self.loadingOff()
                        self.dismiss(animated: true, completion: nil)
                    })
                })
            }), animated: true, completion: nil)
        } else {
            self.present(alertController.twoAction(ftitle: "Warning", fmessage: "Do you want to retire from " + challenge.headLine! + "?", completion: {
                self.loadingOn()
                FireBaseChallenges().retireFromChallenge(id: self.challenge.id!, publicPrivate: self.challenge.publicPrivate!, completion: {
                    self.loadingOff()
                    self.dismiss(animated: true, completion: nil)
                })
            }), animated: true, completion: nil)
        }
    }
    
    func finished()
    {
        backButton.isHidden = true
        chatButton.isHidden = true
        FireBaseChallenges().finishChallenge(id: challenge.id!, count: challenge.stepsDone, publicPrivare: challenge.publicPrivate!, completionHandler: {
            self.loadingOff()
            CATransaction.begin()
            CATransaction.setCompletionBlock({
                self.chatButton.isHidden = false
                self.backButton.isHidden = false
                self.performSegue(withIdentifier: "congrats", sender: self.challenge)
            })
            self.progressBar.setProgress((Float(self.challenge.stepsDone) / Float(self.challenge.stepsToComplete)), animated: true)
            CATransaction.commit()
        })
    }
    
    @IBAction func blockClick(_ sender: Any)
    {
        performSegue(withIdentifier: "block", sender: challenge)
    }
    
    @IBAction func removeClick(_ sender: Any)
    {
        self.present(alertController.twoAction(ftitle: "Warning", fmessage: "Do you want to remove " + challenge.headLine! + "?", completion: {
            self.loadingOn()
            FireBaseChallenges().removeChallenge(id: self.challenge.id!, publicPrivate: self.challenge.publicPrivate!, completion: {
                self.loadingOff()
                self.dismiss(animated: true, completion: nil)
            })
        }), animated: true, completion: nil)
    }
    
    func checkChatSettings()
    {
        if(challenge.chatActive == true)
        {
            changeChatLabel.text = "Current: Chat enabled"
            chatSwitch.setOn(true, animated: false)
        } else {
            changeChatLabel.text = "Current: Chat disabled"
            chatSwitch.setOn(false, animated: false)
        }
        
        checkSwitch()
    }
    
    func checkSwitch()
    {
        if(chatSwitch.isOn == challenge.chatActive)
        {
            saveChatSettingsButton.isHidden = true
        } else {
            saveChatSettingsButton.isHidden = false
        }
    }

    @IBAction func changeChat(_ sender: Any)
    {
        if(chatSwitch.isOn == true)
        {
            changeChatLabel.text = "Current: Chat enabled"
        } else {
            changeChatLabel.text = "Current: Chat disabled"
        }
        checkSwitch()
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "chat")
        {
            let dest = segue.destination as! messagingVC
            let thisChallenge = sender as! Challenge
        
            dest.chatId = thisChallenge.id!
        
            if(challenge.isPrivate == true)
            {
                dest.chatDest = "Private"
            } else {
                dest.chatDest = "Public"
            }
        }
        if(segue.identifier == "congrats")
        {
            let dest = segue.destination as! congratsVC
            dest.completeChallenge = sender as! Challenge
        }
        if(segue.identifier == "block")
        {
            let dest = segue.destination as! blockUserVC
            dest.challenge = sender as! Challenge
        }
    }

}
