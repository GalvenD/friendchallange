//
//  loginVC.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-14.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class loginVC: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {
    
    //Scrollview
    @IBOutlet weak var scrollView: UIScrollView!
    //LogIn TF's
    @IBOutlet weak var logInEmailTF: UITextField!
    @IBOutlet weak var logInPasswordTF: UITextField!
    //Register TF's
    @IBOutlet weak var regEmailTF: UITextField!
    @IBOutlet weak var regUserNameTF: UITextField!
    @IBOutlet weak var regPasswordTF: UITextField!
    @IBOutlet weak var regRepeatPasswordTF: UITextField!
    @IBOutlet weak var registerAccountButton: AddKey!
    @IBOutlet weak var backToLogInButton: AddKey!
    
    @IBOutlet weak var loadingWheel: UIActivityIndicatorView!
    @IBOutlet weak var doneButton: AddKey!
    @IBOutlet weak var loadingLabel: UILabel!

    var scrolled = false
    
    var alertController = AlertController()
    var firebase = FirebaseFunctions()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadIngOn()
    {
        doneButton.isHidden = true
        registerAccountButton.isHidden = true
        loadingLabel.isHidden = false
        loadingWheel.startAnimating()
        backToLogInButton.isHidden = true
        logInEmailTF.isEnabled = false
        logInPasswordTF.isEnabled = false
        regEmailTF.isEnabled = false
        regUserNameTF.isEnabled = false
        regPasswordTF.isEnabled = false
        regRepeatPasswordTF.isEnabled = false
    }
    
    func loadingOff()
    {
        doneButton.isHidden = false
        registerAccountButton.isHidden = false
        loadingLabel.isHidden = true
        loadingWheel.stopAnimating()
        backToLogInButton.isHidden = false
        logInEmailTF.isEnabled = true
        logInPasswordTF.isEnabled = true
        regEmailTF.isEnabled = true
        regUserNameTF.isEnabled = true
        regPasswordTF.isEnabled = true
        regRepeatPasswordTF.isEnabled = true
    }
    
    @IBAction func doneClick(_ sender: UIButton)
    {
        if(scrolled == false)
        {
            logIn()
        } else {
            if(regEmailTF.text != "" && regUserNameTF.text != "" && regPasswordTF.text != "")
            {
                checkUserName()
            } else {
                self.present(alertController.oneActionFunc(ftitle: "Yikes!", fmessage: "You missed something"), animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func registerClick(_ sender: UIButton)
    {
        scroll()
    }
    
    
    func scroll()
    {
        if(scrolled == false)
        {
            scrollView.setContentOffset(CGPoint(x:scrollView.frame.width, y: 0), animated: true)
            scrolled = true
        } else {
            scrollView.setContentOffset(CGPoint(x:0, y: 0), animated: true)
            scrolled = false
        }
    }
    
    func logIn()
    {
        loadIngOn()
        //Resets alertController
        alertController = AlertController()
        //Sign in
        Auth.auth().signIn(withEmail: logInEmailTF.text!, password: logInPasswordTF.text!, completion: {
            (user, error) in
            if(error == nil)
            {
                self.loadingOff()
                self.dismiss(animated: true, completion: nil)
            } else {
                self.present(self.alertController.oneActionFunc(ftitle: "Yikes!", fmessage: error!.localizedDescription), animated: true, completion: {
                    _ in
                    self.loadingOff()
                })
            }
        })
    }
    
    func checkUserName()
    {
        alertController = AlertController()
        doneButton.isHidden = true
        firebase.checkUserName(child1: "Usernames", child2: regUserNameTF.text!,
        completionHandler: {
            exists in
                if(exists.0 == true)
                {
                    print(exists.1)
                    print("finns")
                    self.present(self.alertController.oneActionFunc(ftitle: "Yikes", fmessage: "Username already taken, try another one"), animated: true, completion: {
                        self.doneButton.isHidden = false
                    })
                } else {
                    self.register()
                    print("finns inte")
                }
        })
    }
    
    func register()
    {
        //Resets alertController
        alertController = AlertController()
        loadIngOn()
        //Checks if password is reapeted correctly
        if(regPasswordTF.text! == regRepeatPasswordTF.text!)
        {
            Auth.auth().createUser(withEmail: regEmailTF.text!, password: regPasswordTF.text!, completion: {(user, error) in
                if(error == nil)
                {
                    
                    //Save userinfo to Firebase
                    let FBRef = Database.database().reference()
                    var userInfo = [String : Any]()
                    userInfo = ["username" : self.regUserNameTF.text!, "fbId" : Auth.auth().currentUser!.uid]
                    FBRef.child("Users").child((Auth.auth().currentUser?.uid)!).child("Userinfo").setValue(userInfo)
                    
                    //var usernames = [String : Any]()
                    let usernames = [self.regUserNameTF.text! : Auth.auth().currentUser!.uid] as [String : Any]
                    FBRef.child("Usernames").updateChildValues(usernames)
                    
                    self.loadingOff()
                    //Dismisses LoginVC
                    self.dismiss(animated: true, completion: nil)
                    
                } else {
                    self.present(self.alertController.oneActionFunc(ftitle: "Yikes", fmessage: error!.localizedDescription), animated: true, completion: {
                        _ in
                        self.loadingOff()
                    })
                }
            })
        } else {
            self.present(alertController.oneActionFunc(ftitle: "Yikes!", fmessage: "Password's didn't match"), animated: true, completion: {
                _ in
                self.loadingOff()
                self.regPasswordTF.text = ""
                self.regRepeatPasswordTF.text = ""
                self.regPasswordTF.becomeFirstResponder()
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case logInEmailTF:
            logInPasswordTF.becomeFirstResponder()
        case logInPasswordTF:
            self.view.endEditing(true)
            logIn()
        case regEmailTF:
            regUserNameTF.becomeFirstResponder()
        case regUserNameTF:
            regPasswordTF.becomeFirstResponder()
        case regPasswordTF:
            regRepeatPasswordTF.becomeFirstResponder()
        case regRepeatPasswordTF:
            self.view.endEditing(true)
            register()
        default:
            break
        }
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
