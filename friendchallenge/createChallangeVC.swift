//
//  createChallangeVC.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-27.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class createChallangeVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var titelTF: UITextField!
    @IBOutlet weak var rulesTF: UITextView!
    @IBOutlet weak var stepsLabel: UILabel!
    @IBOutlet weak var invitesPublicLabel: UILabel!
    @IBOutlet weak var stepsClicker: UIStepper!
    
    @IBOutlet weak var chatActiveLabel: UILabel!
    @IBOutlet weak var chatActiveSwitch: UISwitch!
    
    @IBOutlet weak var stepper: UIStepper!
    
    var alertController = AlertController()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        Challenge.placeholderChallenge = Challenge()
        chatActiveLabel.isHidden = true
        chatActiveSwitch.isHidden = true
        chatActiveLabel.alpha = 0
        chatActiveSwitch.alpha = 0
    }

    @IBAction func addSteps(_ sender: Any)
    {
        Challenge.placeholderChallenge.stepsToComplete = Int(stepsClicker.value)
        stepsLabel.text = String(Challenge.placeholderChallenge.stepsToComplete)
        //addRemoveStepTF()
    }
    
    //Invites only/Public switch
    @IBAction func invitesOnlyPublic(_ sender: Any)
    {
        //Sets challenge to public event
        if(Challenge.placeholderChallenge.isPrivate == true)
        {
            Challenge.placeholderChallenge.isPrivate = false
            
            self.chatActiveLabel.isHidden = false
            self.chatActiveSwitch.isHidden = false
            
            //Animates chat settings
            UIView.animate(withDuration: 0.2, animations: {
                self.chatActiveLabel.alpha = 1
                self.chatActiveSwitch.alpha = 1
                self.view.layoutIfNeeded()
            })
            
            invitesPublicLabel.text = "Current: Public event"
        } else { //Sets challenge to Invites only
            Challenge.placeholderChallenge.isPrivate = true
            
            //Animates chat settings
            UIView.animate(withDuration: 0.2, animations: {
                self.chatActiveLabel.alpha = 0
                self.chatActiveSwitch.alpha = 0
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.chatActiveLabel.isHidden = true
                self.chatActiveSwitch.isHidden = true
            })
            
            invitesPublicLabel.text = "Current: Invites only"
        }
    }
    
    /*func addRemoveStepTF()
    {
        let tempTF = UITextField()
        
        tempTF.frame = stepTF.frame
        tempTF.font = stepTF.font
        tempTF.borderStyle = stepTF.borderStyle
        tempTF.autocorrectionType = stepTF.autocorrectionType
        tempTF.keyboardType = stepTF.keyboardType
        tempTF.returnKeyType = stepTF.returnKeyType
        tempTF.clearButtonMode = stepTF.clearButtonMode
        tempTF.contentVerticalAlignment = stepTF.contentVerticalAlignment
        tempTF.delegate = self
        tempTF.placeholder = "kljdhflkjdhflkjsdhflkdjhf"
        scroller.addSubview(tempTF)
        
        stepTFTopCon.constant = stepTFTopCon.constant + stepTF.frame.size.height
    }*/
    @IBAction func chatActive(_ sender: Any)
    {
        if(Challenge.placeholderChallenge.chatActive == false)
        {
            Challenge.placeholderChallenge.chatActive = true
            chatActiveLabel.text = "Current: Chat enabled"
        } else {
            Challenge.placeholderChallenge.chatActive = false
            chatActiveLabel.text = "Current: Chat disabled"
        }
    }
    
    @IBAction func doneClick(_ sender: Any)
    {
        if(titelTF.text == "" || rulesTF.text == "")
        {
            self.present(alertController.oneActionFunc(ftitle: "Yikes!", fmessage: "You've missed something."), animated: true, completion: nil)
        } else {
            Challenge.placeholderChallenge.createdBy = LogInUser.sharedInstance.name
            Challenge.placeholderChallenge.headLine = titelTF.text
            Challenge.placeholderChallenge.rules = rulesTF.text
            
            performSegue(withIdentifier: "invites", sender: self)
        }
    }

    @IBAction func closeClick(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

}
