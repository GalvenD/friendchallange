//
//  friendsVC.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-26.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class friendsVC: UIViewController, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var friendTableView: UITableView!
    @IBOutlet weak var loadingWheel: UIActivityIndicatorView!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var addButton: AddKey!
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.updateFriendList), for: UIControlEvents.valueChanged)
        friendTableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let clickNotification = Notification.Name("rownumber")
        NotificationCenter.default.addObserver(self, selector: #selector(acceptIgnore), name: clickNotification, object: nil)
        updateFriendList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadingOn()
    {
        loadingWheel.startAnimating()
        loadingLabel.isHidden = false
        friendTableView.alpha = 0.5
        friendTableView.isUserInteractionEnabled = false
        addButton.isHidden = true
    }
    
    func loadingOff()
    {
        loadingWheel.stopAnimating()
        loadingLabel.isHidden = true
        friendTableView.alpha = 1
        friendTableView.isUserInteractionEnabled = true
        addButton.isHidden = false
    }
    
    @IBAction func addFriendClick(_ sender: Any)
    {
        performSegue(withIdentifier: "addFriend", sender: nil)
    }
    
    func acceptIgnore(notification : NSNotification)
    {
        let post = notification.object as! NSDictionary
        let number = post.value(forKey: "rowNumber") as! Int
        let accept = post.value(forKey: "accept") as! Bool
        
        print("Number: " + String(number))
        print("Accept: " + String(accept))
        
        FireBaseFriends().acceptIgnore(accept: accept, fbId: LogInUser.sharedInstance.friendRequests[number].friendId!, completionHandler: {
            (worked) in
            if(worked == true)
            {
                print("funkade")
                self.updateFriendList()
            } else {
                print("funkade inte")
            }
        })
    }
    
    func updateFriendList()
    {
        if(refreshControl.isRefreshing == false)
        {
            loadingOn()
        }
        FireBaseFriends().getFriends(completionHandler: {(requests, friends) in
            LogInUser.sharedInstance.friendRequests = requests
            LogInUser.sharedInstance.friends = friends
            self.friendTableView.reloadData()
            self.refreshControl.endRefreshing()
            self.loadingOff()
            print("refreshed")
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(LogInUser.sharedInstance.friendRequests.count == 0)
        {
            return LogInUser.sharedInstance.friends.count
        } else {
            if(section == 0)
            {
                return LogInUser.sharedInstance.friendRequests.count
            } else {
                return LogInUser.sharedInstance.friends.count
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(LogInUser.sharedInstance.friendRequests.count == 0)
        {
            return 1
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(LogInUser.sharedInstance.friendRequests.count == 0)
        {
            return "Friends"
        } else{
            if(section == 0)
            {
                return "Friend Requests"
            } else {
                return "Friends"
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(LogInUser.sharedInstance.friendRequests.count == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "friend") as! FriendCell
            cell.friendLabel.text = LogInUser.sharedInstance.friends[indexPath.row].name
            return cell
        } else {
            if(indexPath.section == 0)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "request") as! RequestCell
                cell.requestLabel.text = LogInUser.sharedInstance.friendRequests[indexPath.row].name
                cell.requestLabel.cornerRadius = 10
                cell.requestLabel.layer.masksToBounds = true
                cell.rowNumber = indexPath.row
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "friend") as! FriendCell
                cell.friendLabel.text = LogInUser.sharedInstance.friends[indexPath.row].name
                return cell
            }
        }
    }
}
