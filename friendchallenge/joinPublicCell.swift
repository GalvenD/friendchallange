//
//  joinPublicCell.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-07-03.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class joinPublicCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var createdByLabel: UILabel!
    @IBOutlet weak var participantsLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
