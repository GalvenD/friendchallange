//
//  FireBaseChallenges.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-07-11.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import Foundation
import Firebase

class FireBaseChallenges
{
    let fbRef = Database.database().reference()
    var firstNr = false
    var nrArray = [UInt32]()
    
    //Upload challenges
    func uploadChallenge(isPrivate : Bool, challengeData : [String : Any], completionHandler: @escaping (String) -> ())
    {
        let publicUpload = fbRef.child("Challenges").child("Public").childByAutoId()
        let privateUpload = fbRef.child("Challenges").child("Private").childByAutoId()
        
        var challengeKey : String?
        
        //Upload public challenge
        if(isPrivate == false)
        {
            publicUpload.setValue(challengeData, withCompletionBlock: {(error, refer) in
                if(error == nil)
                {
                    challengeKey = publicUpload.key
                    completionHandler(challengeKey!)
                } else {
                    self.errorWarning(error: error!)
                }
            })
        } else { //Upload private challenge
            privateUpload.setValue(challengeData, withCompletionBlock: {(error, refer) in
                if(error == nil)
                {
                    challengeKey = privateUpload.key
                    completionHandler(challengeKey!)
                } else {
                    self.errorWarning(error: error!)
                }
            })
        }
    }
    
    //Send challenge invites to friends
    func sendChallengeInvite(isPrivate: Bool, friend : String, challenge : String, title : String)
    {
        let challengeInvite = fbRef.child("Users").child(friend).child("Challenges").child(challenge)
        let inviteValue : [String : Any] = ["State" : 0, "isPrivate" : isPrivate, "Title" : title, "StepsDone" : 0, "Blocked" : false]
        
        challengeInvite.setValue(inviteValue, withCompletionBlock: {(error, refer) in
            if(error == nil)
            {
                
            } else {
                self.errorWarning(error: error!)
            }
        })
    }
    
    //Set challenge to user directory
    func setChallengeToUser(isPrivate: Bool, challenge : String, title: String)
    {
        let userUpload = fbRef.child("Users").child((Auth.auth().currentUser?.uid)!).child("Challenges").child(challenge)
        let userValue : [String : Any] = ["State" : 1, "isPrivate" : isPrivate, "Title" : title, "StepsDone" : 0, "Blocked" : false]
        
        userUpload.setValue(userValue, withCompletionBlock: {(error, refer) in
            if(error == nil)
            {
                
            } else {
                self.errorWarning(error: error!)
            }
        })
    }
    
    //Get challenge keys
    func getChallengeKeys(completion: @escaping ([Challenge], [Challenge], [Challenge]) -> ())
    {
        let userDirectory = fbRef.child("Users").child((Auth.auth().currentUser?.uid)!).child("Challenges")
        userDirectory.observeSingleEvent(of: .value, with: {(snapshot) in
            var pendingCH = [Challenge]()
            var ongoingCH = [Challenge]()
            var finishedCH = [Challenge]()
            
            if(snapshot.exists() == false)
            {
                completion(pendingCH, ongoingCH, finishedCH)
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot]
            {
                let requestData = item.value as! NSDictionary
                let challenge = Challenge()
                
                challenge.state = requestData.value(forKey: "State") as? Int
                challenge.id = item.key
                challenge.isPrivate = requestData.value(forKey: "isPrivate") as! Bool
                challenge.headLine = requestData.value(forKey: "Title") as? String
                challenge.stepsDone = requestData.value(forKey: "StepsDone") as! Int
                challenge.blocked = requestData.value(forKey: "Blocked") as! Bool
                
                if(requestData.value(forKey: "isPrivate") as! Bool == true)
                {
                    challenge.publicPrivate = "Private"
                } else {
                    challenge.publicPrivate = "Public"
                }
                
                self.fbRef.child("Challenges").child(challenge.publicPrivate!).child(item.key).observeSingleEvent(of: .value, with: {(snapshot) in
                    
                    let item = snapshot.value as? NSDictionary
                    challenge.removed = item?.value(forKey: "Removed") as! Bool
                    
                    switch challenge.state! {
                    case 0:
                        challenge.arrayPlacement = pendingCH.count
                        pendingCH.append(challenge)
                    case 1:
                        challenge.arrayPlacement = ongoingCH.count
                        ongoingCH.append(challenge)
                    case 2:
                        challenge.arrayPlacement = finishedCH.count
                        finishedCH.append(challenge)
                    default:
                        return
                    }
                    DispatchQueue.main.async {
                        completion(pendingCH, ongoingCH, finishedCH)
                    }
                })
            }
        })
    }
    
    //Get challenge data
    func getChallengeData(challenge : Challenge, completion: @escaping (Challenge) -> ())
    {
        let returnChallenge = challenge
        
        let privateDirectory = fbRef.child("Challenges").child("Private")
        let publicDirectory = fbRef.child("Challenges").child("Public")
        
        //Private challenges
        if(challenge.isPrivate == true)
        {
            privateDirectory.child(returnChallenge.id!).observeSingleEvent(of: .value, with: {(snapshot) in
                
                let requestData = snapshot.value as? NSDictionary
                
                returnChallenge.rules = requestData?.value(forKey: "Rules") as? String
                returnChallenge.stepsToComplete = requestData?.value(forKey: "Steps") as! Int
                returnChallenge.createdBy = requestData?.value(forKey: "CreatedBy") as? String
                returnChallenge.chatActive = requestData?.value(forKey: "ChatEnabled") as! Bool
                returnChallenge.participantsNr = requestData?.value(forKey: "ParticipantsNr") as! Int
                returnChallenge.completes = requestData?.value(forKey: "Completes") as! Int
                returnChallenge.removed = requestData?.value(forKey: "Removed") as! Bool
                completion(returnChallenge)
            })
        } else {//Public challenges
            publicDirectory.child(returnChallenge.id!).observeSingleEvent(of: .value, with: {(snapshot) in
                
                let requestData = snapshot.value as? NSDictionary
                
                returnChallenge.rules = requestData?.value(forKey: "Rules") as? String
                returnChallenge.stepsToComplete = requestData?.value(forKey: "Steps") as! Int
                returnChallenge.createdBy = requestData?.value(forKey: "CreatedBy") as? String
                returnChallenge.chatActive = requestData?.value(forKey: "ChatEnabled") as! Bool
                returnChallenge.participantsNr = requestData?.value(forKey: "ParticipantsNr") as! Int
                returnChallenge.completes = requestData?.value(forKey: "Completes") as! Int
                returnChallenge.removed = requestData?.value(forKey: "Removed") as! Bool
                completion(returnChallenge)
            })
        }
    }
    
    func getPublicChallenges(completion: @escaping ([Challenge]) -> ())
    {
        nrArray.removeAll()
        var publicCH = [Challenge]()
        let directory = fbRef.child("Challenges").child("Public")
        directory.observeSingleEvent(of: .value, with: {(snapshot) in
            for data in snapshot.children.allObjects as! [DataSnapshot]
            {
                var exists = false
                let tempCH = Challenge()
                let item = data.value as? NSDictionary
                tempCH.headLine = item?.value(forKey: "Title") as? String
                tempCH.id = data.key
                tempCH.rules = item?.value(forKey: "Rules") as? String
                tempCH.createdBy = item?.value(forKey: "CreatedBy") as? String
                tempCH.completes = item?.value(forKey: "Completes") as! Int
                tempCH.chatActive = item?.value(forKey: "ChatEnabled") as! Bool
                tempCH.participantsNr = item?.value(forKey: "ParticipantsNr") as! Int
                tempCH.removed = item?.value(forKey: "Removed") as! Bool
                tempCH.state = 0
                
                for i in LogInUser.sharedInstance.pendingCH
                {
                    if(tempCH.id == i.id)
                    {
                        exists = true
                        print("hittad pending")
                    }
                }
                
                for i in LogInUser.sharedInstance.ongoingCH
                {
                    if(tempCH.id == i.id)
                    {
                        exists = true
                        print("hittad ongoing")
                    }
                }
                
                for i in LogInUser.sharedInstance.finishedCH
                {
                    if(tempCH.id == i.id)
                    {
                        exists = true
                        print("hittad finished")
                    }
                }
                
                if(exists != true && tempCH.removed == false)
                {
                    publicCH.append(tempCH)
                }
            }
            DispatchQueue.main.async {
                completion(publicCH)
            }
        })
    }
    
    //Generates unik random number !!Not in use in version 1.0!!
    func rndNr(max : Int) -> Int
    {
        var rndNr : UInt32?
        if(firstNr == false)
        {
            rndNr = arc4random_uniform(UInt32(max))
            nrArray.append(rndNr!)
            firstNr = true
            return Int(rndNr!)
        } else {
            rndNr = arc4random_uniform(UInt32(max))
            for nr in nrArray
            {
                if(rndNr == nr)
                {
                    rndNr = arc4random_uniform(UInt32(max))
                } else {
                    nrArray.append(rndNr!)
                }
            }
            return Int(rndNr!)
        }
    }
    
    //Accept/Ignore Challenge invite
    func acceptIgnoreChallenge(key : String, isAccepted : Bool, isPrivate : String, completionHandler: @escaping () -> ())
    {
        let userDirectory = fbRef.child("Users").child((Auth.auth().currentUser?.uid)!).child("Challenges").child(key)
        let challengeDirectory = fbRef.child("Challenges").child(isPrivate).child(key)
        
        if(isAccepted == true)
        {
            userDirectory.updateChildValues(["State" : 1])
            challengeDirectory.child("Participants").child((Auth.auth().currentUser?.uid)!).setValue(["Name" : LogInUser.sharedInstance.name!,
                                                                                                      "Id" : LogInUser.sharedInstance.fbUID!])
            challengeDirectory.child("ParticipantsNr").runTransactionBlock({ (snap) -> TransactionResult in
                if let data = snap.value as? Int
                {
                    snap.value = data + 1
                    return TransactionResult.success(withValue: snap)
                } else {
                    return TransactionResult.success(withValue: snap)
                }
            })
            completionHandler()
        } else {
            userDirectory.removeValue()
            completionHandler()
        }
    }
    
    //Get public challenges
    func joinPublic(id: String, title: String, user: String, completion: @escaping () -> ())
    {
        let userDir = fbRef.child("Users").child(user).child("Challenges").child(id)
        let challengeDir = fbRef.child("Challenges").child("Public").child(id).child("Participants").child(user)
        let challengeDirectory = fbRef.child("Challenges").child("Public").child(id)
        let userValue : [String : Any] = ["State" : 1, "isPrivate" : false, "Title" : title, "StepsDone" : 0, "Blocked" : false]
        
        userDir.setValue(userValue)
        challengeDir.setValue(["Name" : LogInUser.sharedInstance.name])
        
        challengeDirectory.child("ParticipantsNr").runTransactionBlock({ (snap) -> TransactionResult in
            if let data = snap.value as? Int
            {
                snap.value = data + 1
                return TransactionResult.success(withValue: snap)
            } else {
                return TransactionResult.success(withValue: snap)
            }
        })
        completion()
    }
    
    func updateSteps(id: String, count: Int, completionHandler: @escaping () -> ())
    {
        //Updates stepsDone in user directory
        let value = ["StepsDone" : count]
        let directory = fbRef.child("Users").child((Auth.auth().currentUser?.uid)!).child("Challenges").child(id)
        directory.updateChildValues(value)
        completionHandler()
    }
    
    func finishChallenge(id: String, count : Int, publicPrivare : String, completionHandler: @escaping () -> ())
    {
        //Updates challenge in user directory
        let value = ["State" : 2, "StepsDone" : count]
        let directory = fbRef.child("Users").child((Auth.auth().currentUser?.uid)!).child("Challenges").child(id)
        directory.updateChildValues(value)
        
        //Update completesnumber in challenge directory
        let update = fbRef.child("Challenges").child(publicPrivare).child(id).child("Completes")
        update.runTransactionBlock({(snap) -> TransactionResult in
            if let data = snap.value as? Int
            {
                snap.value = data + 1
                return TransactionResult.success(withValue: snap)
            } else {
                return TransactionResult.success(withValue: snap)
            }
        })
        completionHandler()
    }
    
    func getParticipants(id: String, publicPrivate : String, completion: @escaping () -> ())
    {
        Participant.sharedInstance.participant.removeAll()
        
        let directory = fbRef.child("Challenges").child(publicPrivate).child(id).child("Participants")
        directory.observeSingleEvent(of: .value, with: {(snapshot) in
            
            let request = snapshot.children.allObjects as! [DataSnapshot]
            for data in request
            {
                let item = data.value as? NSDictionary
                let temp = Participant()
                
                temp.name = item?.value(forKey: "Name") as? String
                temp.id = item?.value(forKey: "Id") as? String
                Participant.sharedInstance.participant.append(temp)
            }
            completion()
        })
    }
    
    func blockUser(uID : String, cId : String, publicPrivate : String, completion: @escaping () -> ())
    {
        //User dir value
        let value = ["Blocked" : true]
        let userDirectory = fbRef.child("Users").child(uID).child("Challenges").child(cId)
        userDirectory.updateChildValues(value)
        
        //Challenge dir values
        let blockedValue = ["Uid" : uID]
        let challengePartDir = fbRef.child("Challenges").child(publicPrivate).child(cId).child("Participants").child(uID)
        let blockedDir = fbRef.child("Challenges").child(publicPrivate).child(cId).child("BlockedUsers").child(uID)
        challengePartDir.removeValue()
        blockedDir.setValue(blockedValue)
        
        completion()
    }
    
    func changeChatSettings(value: [String : Any], id: String, publicPrivate: String)
    {
        let directory = fbRef.child("Challenges").child(publicPrivate).child(id)
        directory.updateChildValues(value)
    }
    
    //Retire from challenge
    func retireFromChallenge(id : String, publicPrivate : String,completion: @escaping () -> ())
    {
        let userDirectory = fbRef.child("Users").child((Auth.auth().currentUser?.uid)!).child("Challenges").child(id)
        userDirectory.removeValue()
        
        let challengeDirectory = fbRef.child("Challenges").child(publicPrivate).child(id).child("Participants").child((Auth.auth().currentUser?.uid)!)
        challengeDirectory.removeValue()
        
        completion()
    }
    
    //RemoveChallenge
    func removeChallenge(id: String, publicPrivate : String, completion: @escaping () -> ())
    {
        let value = ["Removed" : true]
        let directory = fbRef.child("Challenges").child(publicPrivate).child(id)
        let removedDirectory = fbRef.child("Challenges").child("Removed").child(id)
        directory.updateChildValues(value)
        removedDirectory.setValue(["Location" : publicPrivate])
        completion()
    }
    
    //Errorhandeling
    func errorWarning(error: Error)
    {
        print("Error: " + error.localizedDescription)
    }
}
