//
//  recieveCell.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-30.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class recieveCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
