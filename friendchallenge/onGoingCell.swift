//
//  onGoingCell.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-29.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class onGoingCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var publicPrivateLabel: UILabel!
    @IBOutlet weak var blockedLabel: UILabel!
    
    var blocked : Bool?
    var removed : Bool?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUp()
    {
        if(blocked == true)
        {
            blockedLabel.isHidden = false
            blockedLabel.text = "blocked"
        }
        if(removed == true && blocked == false)
        {
            blockedLabel.isHidden = false
            blockedLabel.text = "removed"
        }
        else
        {
            blockedLabel.isHidden = true
        }
    }
}
