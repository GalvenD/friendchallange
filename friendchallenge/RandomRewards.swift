//
//  RandomRewards.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-07-04.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import Foundation
class RandomRewards
{
    let randomRewards = ["riding an elephant", "petting a dolphin", "riding an electric bike",
                         "eating something nice", "don't doing your laundry", "eating a piece of chocolate", "singing a boy band song", "drinking something nice",
                         "going on a fishing trip", "calling an old friend", "giving yourself a high five", "writing a poem", "watching some TV", "going to the movies", "don't doing your dishes", "moonwalking",
                         "sitting down and relax", "doing nothing", "listening to some music"]
    
    func randomText() -> String
    {
        let rndNR = arc4random_uniform(UInt32(randomRewards.count))
        
        let text = "Celebrate by " + randomRewards[Int(rndNR)]
        
        return text
    }
}
