//
//  startVC.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-14.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class startVC: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLAbel: UILabel!
    @IBOutlet weak var chCompleted: UILabel!
    @IBOutlet weak var chTotal: UILabel!
    @IBOutlet weak var friendsLabel: UILabel!
    
    @IBOutlet weak var loadingWheel: UIActivityIndicatorView!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var statsView: UIView!
    
    var userData = false
    var friends = false
    var keys = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        statsView.layer.cornerRadius = 10
    }

    override func viewDidAppear(_ animated: Bool) {
        checkIfLoggedIn()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //checkIfLoggedIn()
    }
    
    func loadingOn()
    {
        loadingWheel.startAnimating()
        loadingLabel.isHidden = false
        nameLabel.isHidden = true
        emailLAbel.isHidden = true
        statsView.isHidden = true
    }
    
    func loadingOff()
    {
        if(userData == true && friends == true && keys == true)
        {
            loadingWheel.stopAnimating()
            loadingLabel.isHidden = true
            nameLabel.isHidden = false
            emailLAbel.isHidden = false
            statsView.isHidden = false
        }
    }
    
    func checkIfLoggedIn()
    {
        loadingOn()
        print("kollar")
        if(Auth.auth().currentUser == nil)
        {
            performSegue(withIdentifier: "tologin", sender: nil)
        } else {
            print("hämtar")
            getUserData()
            getChallengeKeys()
            getFriends()
            loadingOff()
        }
    }
    
    func getUserData()
    {
        FirebaseFunctions().setUserData(completionHandler: { done in
            if(done == true)
            {
                print("Name: " + LogInUser.sharedInstance.name!)
                print("UID: " + LogInUser.sharedInstance.fbUID!)
                self.nameLabel.text = LogInUser.sharedInstance.name
                self.emailLAbel.text = Auth.auth().currentUser?.email
                self.userData = true
                self.loadingOff()
            } else {
                print("något gick fel")
            }
        })
    }
    
    func getChallengeKeys()
    {
        FireBaseChallenges().getChallengeKeys {(pending, ongoing, finished) in
           LogInUser.sharedInstance.ongoingCH = ongoing
            LogInUser.sharedInstance.finishedCH = finished
            self.chCompleted.text = "Challenges completed: " + String(LogInUser.sharedInstance.finishedCH.count)
            self.chTotal.text = "Challenges total: " + String(LogInUser.sharedInstance.finishedCH.count + LogInUser.sharedInstance.ongoingCH.count)
            self.keys = true
            self.loadingOff()
        }
    }
    
    func getFriends()
    {
        FireBaseFriends().getFriends(completionHandler: {(pending, friends) in
            LogInUser.sharedInstance.friendRequests = pending
            LogInUser.sharedInstance.friends = friends
            self.friendsLabel.text = "Friends: " +  String(LogInUser.sharedInstance.friends.count)
            self.friends = true
            self.loadingOff()
        })
    }
    
    @IBAction func signOutClick(_ sender: Any)
    {
        FirebaseFunctions().signOut {
            self.checkIfLoggedIn()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
