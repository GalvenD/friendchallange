//
//  blockUserVC.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-07-01.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class blockUserVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var loadingWheel: UIActivityIndicatorView!
    @IBOutlet weak var doneButton: AddKey!
    var challenge = Challenge()
    var alertController = AlertController()
    
    @IBOutlet weak var blockTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let clickNotification = Notification.Name("blockNumber")
        NotificationCenter.default.addObserver(self, selector: #selector(blockUser), name: clickNotification, object: nil)
        getParticipants()
    }
    
    func loadingOn()
    {
        loadingWheel.startAnimating()
        loadingLabel.isHidden = false
        blockTableView.alpha = 0.4
        blockTableView.isUserInteractionEnabled = false
    }
    
    func loadingOff()
    {
        loadingWheel.stopAnimating()
        loadingLabel.isHidden = true
        blockTableView.alpha = 1
        blockTableView.isUserInteractionEnabled = true
    }
    
    func getParticipants()
    {
        loadingOn()
        FireBaseChallenges().getParticipants(id: challenge.id!, publicPrivate: challenge.publicPrivate!, completion: {
            self.loadingOff()
            self.blockTableView.reloadData()
            print(String(Participant.sharedInstance.participant.count))
        })
    }
    
    func blockUser(notification : Notification)
    {
        let post = notification.object as? NSDictionary
        let rowNumber = post?.value(forKey: "blockNumber") as! Int
        loadingOn()
        self.present(alertController.twoAction(ftitle: "Warning", fmessage: "Do you want to block " + Participant.sharedInstance.participant[rowNumber].name! + "?", completion: {
            FireBaseChallenges().blockUser(uID: Participant.sharedInstance.participant[rowNumber].id!, cId: self.challenge.id!, publicPrivate: self.challenge.publicPrivate!, completion: {
                self.getParticipants()
            })
        }), animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return Participant.sharedInstance.participant.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "blockcell", for: indexPath) as! blockCell
        cell.nameLabel.text = Participant.sharedInstance.participant[indexPath.row].name
        cell.rowNumber = indexPath.row
        return cell
    }
    
    @IBAction func doneClick(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewWillDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
