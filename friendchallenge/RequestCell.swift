//
//  RequestCell.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-26.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class RequestCell: UITableViewCell
{

    @IBOutlet weak var requestLabel: FriendLabel!
    var rowNumber : Int?
    
    @IBAction func acceptClick(_ sender: Any)
    {
        sendRow(accept: true)
    }
    
    @IBAction func ignoreClick(_ sender: Any)
    {
        sendRow(accept: false)
    }
    
    func sendRow(accept : Bool)
    {
        let passRownumber = Notification.Name("rownumber")
        let message = ["rowNumber" : rowNumber as Any, "accept" : accept]
        NotificationCenter.default.post(name: passRownumber, object: message)
    }
}
