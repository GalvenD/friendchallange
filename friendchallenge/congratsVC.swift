//
//  congratsVC.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-07-01.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class congratsVC: UIViewController {
    
    var completeChallenge = Challenge()
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var challengeLabel: UILabel!
    @IBOutlet weak var celebrateLabel: UILabel!
    
    var reward = RandomRewards()
    override func viewDidLoad() {
        super.viewDidLoad()
        celebrateLabel.sizeToFit()
        parentView.layer.cornerRadius = 10
        challengeLabel.text = completeChallenge.headLine!
        celebrateLabel.text = reward.randomText()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func closeC(_ sender: Any)
    {
        self.presentingViewController!.presentingViewController!.dismiss(animated: true, completion: {})
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
