//
//  Message.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-14.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import Foundation

class Message
{
    static var placeholderMessage = Message()
    
    var id : String?
    var sentBy : String?
    var text : String?
}
