//
//  challengesViewController.swift
//  friendchallenge
//
//  Created by Dennis Galvén on 2017-06-14.
//  Copyright © 2017 GalvenD. All rights reserved.
//

import UIKit

class challengesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var challengeTV: UITableView!
    @IBOutlet weak var loadWheel: UIActivityIndicatorView!
    @IBOutlet weak var loadLabel: UILabel!
    @IBOutlet weak var addButton: AddKey!
    var refreshControl : UIRefreshControl!
    @IBOutlet weak var joinButton: AddKey!
    @IBOutlet weak var menuButton: UIButton!
    
    //Addkey constraints
    @IBOutlet weak var addWidth: NSLayoutConstraint!
    @IBOutlet weak var addHeight: NSLayoutConstraint!
    @IBOutlet weak var addTrailing: NSLayoutConstraint!
    
    //Joinkey constraints
    @IBOutlet weak var joinWidth: NSLayoutConstraint!
    @IBOutlet weak var joinHeight: NSLayoutConstraint!
    @IBOutlet weak var joinBottom: NSLayoutConstraint!
    
    //Menupresented check
    var menuPresented = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.getChallenges), for: UIControlEvents.valueChanged)
        challengeTV.addSubview(refreshControl)
        resetMenu()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let clickNotification = Notification.Name("rowNumber")
        NotificationCenter.default.addObserver(self, selector: #selector(acceptIgnore), name: clickNotification, object: nil)
        getChallenges()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuClick(_ sender: Any)
    {
        animateMenu()
    }
    
    func animateMenu()
    {
        if(menuPresented == false)
        {
            addButton.isHidden = false
            joinButton.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.addWidth.constant = 50
                self.addHeight.constant = 50
                self.addTrailing.constant = 70
                self.addButton.alpha = 1
                
                self.joinWidth.constant = 50
                self.joinHeight.constant = 50
                self.joinBottom.constant = 70
                self.joinButton.alpha = 1
                
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.addButton.isEnabled = true
                self.joinButton.isEnabled = true
                self.menuPresented = true
            })
        } else {
            addButton.isEnabled = false
            joinButton.isEnabled = false
            UIView.animate(withDuration: 0.3, animations: {
                self.addWidth.constant = 30
                self.addHeight.constant = 30
                self.addTrailing.constant = 20
                self.addButton.alpha = 0
                
                self.joinWidth.constant = 30
                self.joinHeight.constant = 30
                self.joinBottom.constant = 20
                self.joinButton.alpha = 0
                
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.addButton.isHidden = true
                self.joinButton.isHidden = true
                self.menuPresented = false
            })
        }
    }
    
    func resetMenu()
    {
        menuPresented = false
        //Join/Add-key start positions
        addWidth.constant = 30
        addHeight.constant = 30
        addTrailing.constant = 20
        addButton.alpha = 0
        addButton.isHidden = true
        addButton.isEnabled = false
        joinWidth.constant = 30
        joinHeight.constant = 30
        joinBottom.constant = 20
        joinButton.alpha = 0
        joinButton.isHidden = true
        joinButton.isEnabled = false
    }
    
    func loadingOn()
    {
        challengeTV.alpha = 0.5
        challengeTV.isUserInteractionEnabled = false
        menuButton.isHidden = true
        loadWheel.startAnimating()
        loadLabel.isHidden = false
    }
    
    func loadingOff()
    {
        challengeTV.alpha = 1
        challengeTV.isUserInteractionEnabled = true
        menuButton.isHidden = false
        loadWheel.stopAnimating()
        loadLabel.isHidden = true
    }
    
    func acceptIgnore(notification : NSNotification)
    {
        let post = notification.object as! NSDictionary
        let rowNumber = post.value(forKey: "rowNumber") as! Int
        let accept = post.value(forKey: "accept") as! Bool
        
        loadingOn()
        
        if(LogInUser.sharedInstance.pendingCH[rowNumber].isPrivate == true)
        {
            FireBaseChallenges().acceptIgnoreChallenge(key: LogInUser.sharedInstance.pendingCH[rowNumber].id!,isAccepted: accept, isPrivate: "Private", completionHandler: {
                self.getChallenges()
            })
        } else {
            FireBaseChallenges().acceptIgnoreChallenge(key: LogInUser.sharedInstance.pendingCH[rowNumber].id!,isAccepted: accept, isPrivate: "Public", completionHandler: {
                self.getChallenges()
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(LogInUser.sharedInstance.pendingCH.count == 0)
        {
            return LogInUser.sharedInstance.ongoingCH.count
        } else {
            if(section == 0)
            {
                return LogInUser.sharedInstance.pendingCH.count
            } else {
                return LogInUser.sharedInstance.ongoingCH.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(LogInUser.sharedInstance.pendingCH.count == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "challenge", for: indexPath) as! onGoingCell
            cell.blocked = LogInUser.sharedInstance.ongoingCH[indexPath.row].blocked
            cell.removed = LogInUser.sharedInstance.ongoingCH[indexPath.row].removed
            cell.publicPrivateLabel.text = LogInUser.sharedInstance.ongoingCH[indexPath.row].publicPrivate
            cell.titleLabel.text = LogInUser.sharedInstance.ongoingCH[indexPath.row].headLine
            cell.setUp()
            return cell
        } else {
            if(indexPath.section == 0)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "pending", for: indexPath) as! pendingCell
                cell.publicPrivateLabel.text = LogInUser.sharedInstance.pendingCH[indexPath.row].publicPrivate!
                cell.titleLabel.text = LogInUser.sharedInstance.pendingCH[indexPath.row].headLine!
                cell.rowNumber = indexPath.row
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "challenge", for: indexPath) as! onGoingCell
                cell.blocked = LogInUser.sharedInstance.ongoingCH[indexPath.row].blocked
                cell.removed = LogInUser.sharedInstance.ongoingCH[indexPath.row].removed
                cell.publicPrivateLabel.text = LogInUser.sharedInstance.ongoingCH[indexPath.row].publicPrivate
                cell.titleLabel.text = LogInUser.sharedInstance.ongoingCH[indexPath.row].headLine
                cell.setUp()
                return cell
            }
        }
    }
    
    func getChallenges()
    {
        if(refreshControl.isRefreshing == false)
        {
            loadingOn()
        }
        FireBaseChallenges().getChallengeKeys(completion: {(pending, ongoing, finished) in
            LogInUser.sharedInstance.pendingCH = pending.reversed()
            LogInUser.sharedInstance.ongoingCH = ongoing.reversed()
            LogInUser.sharedInstance.finishedCH = finished.reversed()
            self.challengeTV.reloadData()
            self.refreshControl.endRefreshing()
            self.loadingOff()
        })
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        loadingOn()
        if(LogInUser.sharedInstance.pendingCH.count == 0)
        {
            FireBaseChallenges().getChallengeData(challenge: LogInUser.sharedInstance.ongoingCH[indexPath.row], completion: {(challenge) in
                self.loadingOff()
                self.resetMenu()
                self.performSegue(withIdentifier: "detail", sender: challenge)
            })
        } else {
            if(indexPath.section == 0)
            {
                FireBaseChallenges().getChallengeData(challenge: LogInUser.sharedInstance.pendingCH[indexPath.row], completion: {(challenge) in
                    self.loadingOff()
                    self.resetMenu()
                    self.performSegue(withIdentifier: "detail", sender: challenge)
                })
            } else {
                FireBaseChallenges().getChallengeData(challenge: LogInUser.sharedInstance.ongoingCH[indexPath.row], completion: {(challenge) in
                    self.loadingOff()
                    self.resetMenu()
                    self.performSegue(withIdentifier: "detail", sender: challenge)
                })
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(LogInUser.sharedInstance.pendingCH.count == 0)
        {
            return "Ongoing Challenges"
        } else {
            if(section == 0)
            {
                return "Challenge Invites"
            } else {
                return "Ongoing Challenges"
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(LogInUser.sharedInstance.pendingCH.count == 0)
        {
            return 1
        } else {
            return 2
        }
    }
    
    @IBAction func joinClick(_ sender: Any)
    {
        resetMenu()
        performSegue(withIdentifier: "public", sender: nil)
    }
    
    @IBAction func addClick(_ sender: UIButton)
    {
        resetMenu()
        performSegue(withIdentifier: "tocreate", sender: nil)
    }
    
    @IBAction func backToChallenge(segue: UIStoryboardSegue)
    {
        
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "detail")
        {
            let dest = segue.destination as! detailChallengeVC
            dest.challenge = sender as! Challenge
        }
    }
}
